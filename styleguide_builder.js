/**
 * @file
 * Code for the styleguide_builder module.
 */

(function (document, $, settings) {

  'use strict';

  var scrollTo = function (identifier, complete) {
    $('body').stop().animate({
      scrollTop: $(identifier).offset().top
    }, 400, complete);
  };

  Drupal.behaviors.styleguide_builder = {

    attach: function (context) {

      this.scrollTo(context);
      this.fieldsetToggle(context);
      this.fieldsetCollapseAll(context);

    },

    /**
     * Style guide page scrollTo.
     *
     * @param context
     *  Contains the DOM.
     */
    scrollTo: function (context) {

      var $links = $('#' + settings.styleguideBuilder.menuName + ' a', context);

      $links
        .removeClass('active')
        .click(function (evt) {
          evt.preventDefault();

          var $self = $(this);
          var href = $self.attr('href');
          var fragment = href.substr(href.indexOf('#'));

          $links
            .removeClass('active');

          // User is collapsing the menu; scrollTo just irritates in that case.
          // @see Drupal.behaviors.styleguide_builder_theme.menuCollapsible()
          if (settings.styleguideBuilder.menuCollapsible && $self.parent('li').hasClass('expanded')) {
            return;
          }

          scrollTo(fragment, function () {
            document.location.hash = fragment;

            $self
              .addClass('active');

            var $code = $(fragment, context)
              .focus()
              .nextUntil('fieldset.code')
              .next('fieldset.code');

            if (!$code.length) {
              $code = $(fragment, context)
                .focus()
                .next('fieldset.code');
            }
            if ($code.length) {
              $code
                .addClass('show')
                .children('legend')
                .click();
            }
          });
        });

    },

    /**
     * Expand/collapse a fieldset.
     *
     * @param context
     *  Contains the DOM.
     */
    fieldsetToggle: function (context) {

      $('.style-guide-elements fieldset > legend', context)
        .click(function (evt) {
          evt.preventDefault();

          var $fieldset = $(this).parent('fieldset');
          var $content = $('.content', $fieldset);

          if ($content.length) {
            if ($content.is(':not(:hidden)')) {
              if (!$fieldset.hasClass('show')) {
                $content.slideUp();
              }
            }
            else {
              // $('#style-guide a.collapse', context).click();
              $content.slideDown();
            }

            $fieldset.removeClass('show');
          }
        });

    },

    /**
     * Collapse all fieldsets.
     *
     * @param context
     *  Contains the DOM.
     */
    fieldsetCollapseAll: function (context) {

      $('#style-guide a.collapse', context)
        .click(function (evt) {
          evt.preventDefault();

          $('.style-guide-elements fieldset .content', context)
            .slideUp();
        });

    }

  };

  Drupal.behaviors.styleguide_builder_theme = {

    attach: function (context) {

      if (settings.styleguideBuilder.menuSticky) {
        this.menuSticky(context, settings);
      }
      if (settings.styleguideBuilder.menuCollapsible) {
        this.menuCollapsible(context, settings);
      }
      if (settings.styleguideBuilder.pagedSections) {
        this.pagedSections(context, settings);
      }

    },

    /**
     * Sticky menu.
     *
     * @param context
     *  Contains the DOM.
     */
    menuSticky: function (context) {

      var $menu = $('.style-guide-nav', context);
      var $content = $('.style-guide-elements', context);

      $menu.addClass('sticky');
      $content.addClass('sticky');

      var $sticky = $('#' + settings.styleguideBuilder.menuName + ' > ul.menu', context)
        .addClass('sticky');

      var $stopper = $(document.createElement('div'))
        .addClass('sticky-stopper').css({
          position: 'absolute',
          bottom: 0
        });

      $sticky
        .parent()
        .append($stopper);

      var height = $sticky.innerHeight();
      var stickyTop = $sticky.offset().top;
      var stickOffset = 100;
      var stickyStopperPosition = $stopper.offset().top;
      var stopPoint = (stickyStopperPosition - height - stickOffset);
      var diff = (stopPoint + stickOffset);

      $(window)
        .scroll(function () {
          var windowTop = $(window).scrollTop();

          if (stopPoint < windowTop) {
            $sticky.css({
              position: 'absolute',
              top: diff
            });
          }
          else if (stickyTop < (windowTop + stickOffset)) {
            $sticky.css({
              position: 'fixed',
              top: stickOffset
            });
          }
          else {
            $sticky.css({
              position: 'absolute',
              top: 'initial'
            });
          }
        });

    },

    /**
     * Collapsible menu.
     *
     * @param context
     *  Contains the DOM.
     */
    menuCollapsible: function (context) {

      var $menu = $('#' + settings.styleguideBuilder.menuName + ' > ul.menu', context);

      $('ul.menu > li', $menu)
        .hide();

      $('a', $menu)
        .click(function (evt) {
          var $self = $(this);

          var $subMenu = $self
            .siblings('ul.menu');

          if ($subMenu.length) {
            $self.parent('li')
              .toggleClass('expanded')
              .toggleClass('collapsed');
            $subMenu
              .children('li')
              .toggle();
          }
        });
    },

    /**
     * Make sections behave as separate pages.
     *
     * @param context
     *  Contains the DOM.
     */
    pagedSections: function (context) {

      var $links = $('#' + settings.styleguideBuilder.menuName + ' a', context);
      var $sections = $('.style-guide-elements > section', context)
        .hide();

      $sections
        .first()
        .show();

      $links.click(function (evt) {
        var $self = $(this);
        var href = $self.attr('href');
        var target = href.substr(href.indexOf('#'));

        $sections
          .hide();

        $(target, $sections)
          .parents('section')
          .show();
      });

    }

  };

})(document, jQuery, Drupal.settings);
