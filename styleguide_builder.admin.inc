<?php

/**
 * @file
 * Code for the styleguide_builder module.
 */

if (file_exists(variable_get('composer_manager_vendor_dir', 'sites/all/vendor') . '/autoload.php')) {
  require_once variable_get('composer_manager_vendor_dir', 'sites/all/vendor') . '/autoload.php';
}

use Symfony\Component\Yaml\Parser;

/**
 * Settings form.
 */
function styleguide_builder_settings_form() {

  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#collapsible' => FALSE,
  );

  $form['settings']['styleguide_builder_menu'] = array(
    '#type' => 'select',
    '#title' => t('Menu'),
    '#description' => t('The menu that lists style guide elements.'),
    '#required' => TRUE,
    '#options' => menu_get_menus(),
    '#default_value' => variable_get('styleguide_builder_menu', NULL),
  );

  $form['settings']['styleguide_builder_theme'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
  );

  $vars_theme = variable_get('styleguide_builder_theme', array(
    'menu_sticky' => FALSE,
    'menu_collapsible' => FALSE,
    'paged_sections' => FALSE,
  ));

  $form['settings']['styleguide_builder_theme']['description'] = array(
    '#type' => 'item',
    '#title' => t('Theme options'),
    '#description' => t("!cache if these options don't seem to take effect immediately.", array(
      '!cache' => l(t('Clear all caches'), 'admin/config/development/performance'),
    )),
  );

  $form['settings']['styleguide_builder_theme']['menu_sticky'] = array(
    '#type' => 'checkbox',
    '#title' => t('The menu is sticky.'),
    '#default_value' => $vars_theme['menu_sticky'],
  );

  $form['settings']['styleguide_builder_theme']['menu_collapsible'] = array(
    '#type' => 'checkbox',
    '#title' => t('The menu is collapsible.'),
    '#default_value' => $vars_theme['menu_collapsible'],
  );

  $form['settings']['styleguide_builder_theme']['paged_sections'] = array(
    '#type' => 'checkbox',
    '#title' => t('The top-level menu elements are treated as separate pages.'),
    '#default_value' => $vars_theme['paged_sections'],
  );

  return system_settings_form($form);
}

/**
 * Import form.
 */
function styleguide_builder_import_form($form, &$form_state) {

  if (!class_exists('\Symfony\Component\Yaml\Parser')) {
    drupal_set_message(t('Module dependencies have not been met. Please complete the !manager instructions before proceeding.', array(
      '!manager' => l(t('Composer Manager'), 'admin/config/system/composer-manager'),
    )), 'warning', FALSE);

    return;
  }

  drupal_set_message(t('Submitting this form will purge all existing elements from the <code>@menu</code> menu.', array(
    '@menu' => variable_get('styleguide_builder_menu', NULL),
  )), 'warning', FALSE);

  $form['wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t('Import elements'),
    '#collapsible' => FALSE,
  );

  $form['wrapper']['import'] = array(
    '#type' => 'file',
    '#title' => t('Elements file'),
    '#description' => t('The YAML file containing your style guide elements. Example: !example', array(
      '!example' => l(t('styleguide_builder.menu_links.yml'), drupal_get_path('module', 'styleguide_builder') . '/config/optional/styleguide_builder.menu_links.yml'),
    )),
  );

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Import elements'));

  return $form;
}

/**
 * Submit handler for import form.
 */
function styleguide_builder_import_form_submit($form, &$form_state) {

  $destination = 'public://styleguide_builder/';
  file_prepare_directory($destination, FILE_CREATE_DIRECTORY);

  $file = file_save_upload('import', array(
    'file_validate_extensions'    => array('yml'),
    'custom_validate_size' => array(1024 * 1024 * 30),
  ),
    $destination
  );
  $url = parse_url(file_create_url($file->uri));
  $path = trim($url['path'], '/');

  _styleguide_builder_import_init($path);
  file_delete($file);
  drupal_set_message(t('Style guide elements have been successfully imported.'));

}

/**
 * Initializes menu tree import.
 */
function _styleguide_builder_import_init($file_path) {

  $parser = new Parser();
  $data = file_get_contents($file_path);
  $data = $parser->parse($data);

  _styleguide_builder_import($data);
}

/**
 * Helper function to import the menu tree.
 */
function _styleguide_builder_import(&$data, $plid = 0) {

  $args = &drupal_static(__FUNCTION__);
  if (empty($args)) {
    $args['menu_name'] = variable_get('styleguide_builder_menu', NULL);

    // Delete all links from the menu.
    menu_delete_links($args['menu_name']);
  }

  $weight = 0;
  foreach ($data as $stub) {
    $item = array(
      'link_path' => 'style-guide',
      'link_title' => $stub['title'],
      'menu_name' => $args['menu_name'],
      'weight' => $weight,
      'plid' => $plid,
      'module' => 'styleguide_builder',
      'options' => array(
        'styleguide_builder' => array(),
      ),
    );

    if (!empty($stub['description'])) {
      $item['options']['styleguide_builder']['description'] = $stub['description'];
    }
    if (!empty($stub['code'])) {
      $item['options']['styleguide_builder']['code'] = array(
        'html' => (!empty($stub['code']['html']) ? $stub['code']['html'] : ''),
        'css' => (!empty($stub['code']['css']) ? $stub['code']['css'] : ''),
        'js' => (!empty($stub['code']['js']) ? $stub['code']['js'] : ''),
      );
    }

    menu_link_save($item);
    $weight++;

    if (!empty($stub['below'])) {
      _styleguide_builder_import($stub['below'], $item['mlid']);
    }
  }

}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function styleguide_builder_form_menu_edit_menu_alter(&$form, &$form_state, $form_id) {

  // Only alter the form if this item belongs to configured menu.
  $menu_name = variable_get('styleguide_builder_menu', '');
  if ($form_state['build_info']['args'][1]['menu_name'] !== $menu_name) {
    return;
  }

  $form['actions']['delete']['#access'] = FALSE;
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function styleguide_builder_form_menu_edit_item_alter(&$form, &$form_state, $form_id) {

  // Only alter the form if this item belongs to configured menu.
  $menu_name = variable_get('styleguide_builder_menu', '');
  $menu_item = NULL;
  $item_menu_name = '';
  if (!empty($form_state['build_info']['args'][1]['menu_name'])) {
    $menu_item = $form_state['build_info']['args'][1];
    $item_menu_name = $menu_item['menu_name'];
  }
  elseif (!empty($form_state['build_info']['args'][2]['menu_name'])) {
    $item_menu_name = $form_state['build_info']['args'][2]['menu_name'];
  }
  if ($item_menu_name !== $menu_name) {
    return;
  }

  drupal_set_message(t('Set the properties for this style guide element.'));

  $form['#attached']['css'][] = drupal_get_path('module', 'styleguide_builder') . '/styleguide_builder.css';

  // Add extra submit handler.
  $form['#submit'][] = 'styleguide_builder_form_menu_edit_item_submit';

  // Force a fixed path for menu items.
  $form['link_path'] = array(
    '#type' => 'value',
    '#value' => 'style-guide',
  );

  $form['_path']['#access'] = FALSE;
  $form['weight']['#access'] = FALSE;

  $form['link_title']['#title'] = t('Title');
  $form['link_title']['#description'] = t('The text to be used for the menu link and style guide heading.');

  $form['description']['#weight'] = 90;
  $form['description']['#rows'] = 5;
  $form['description']['#resizable'] = FALSE;
  $form['description']['#description'] = t('The description will appear below the style guide heading.');

  // Reduce the available parent menu item options.
  $parent_options = &$form['parent']['#options'];
  foreach ($parent_options as $key => $label) {
    if (stripos($key, $menu_name) !== 0) {
      unset($parent_options[$key]);
    }
  }

  // Add some extra fields.
  $form['styleguide_builder'] = array(
    '#type' => 'fieldset',
    '#title' => t('Code'),
    '#description' => t('Define the code properties for this style guide element.'),
    '#weight' => 100,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#tree' => TRUE,
    '#attributes' => array(
      'class' => array('code'),
    ),
  );

  $form['styleguide_builder']['code']['html'] = array(
    '#type' => 'textarea',
    '#title' => t('HTML'),
  );

  $form['styleguide_builder']['code']['css'] = array(
    '#type' => 'textarea',
    '#title' => t('CSS'),
  );

  $form['styleguide_builder']['code']['js'] = array(
    '#type' => 'textarea',
    '#title' => t('JavaScript'),
  );

  $form['actions']['#weight'] = 110;

  // Set #default_value for elements.
  if (!empty($menu_item)) {
    if (!empty($menu_item['options']['styleguide_builder']['description'])) {
      $form['description']['#default_value'] = $menu_item['options']['styleguide_builder']['description'];
    }

    $collapse = TRUE;
    if (!empty($menu_item['options']['styleguide_builder']['code'])) {
      foreach (array_keys($menu_item['options']['styleguide_builder']['code']) as $key) {
        $form['styleguide_builder']['code'][$key]['#default_value'] = $menu_item['options']['styleguide_builder']['code'][$key];

        if (!empty($menu_item['options']['styleguide_builder']['code'][$key])) {
          $collapse = FALSE;
        }
      }
    }

    $form['styleguide_builder']['#collapsed'] = $collapse;
  }

}

/**
 * Submit handler for menu item form.
 */
function styleguide_builder_form_menu_edit_item_submit($form, &$form_state) {

  // Menu module updates values by reference, so this is a fully populated item.
  $item = &$form_state['values'];

  unset($item['options']['attributes']['title']);

  $item['module'] = 'styleguide_builder';
  $item['options']['styleguide_builder'] = $item['styleguide_builder'];
  $item['options']['styleguide_builder']['description'] = $item['description'];

  menu_link_save($item);

}
