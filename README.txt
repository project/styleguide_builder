# Style Guide Builder

        /**
         * Rebuilds the style guide elements.
         */
        function styleguide_builder_update_7100() {

          $path = drupal_get_path('module', 'styleguide_builder') . '/config/optional/styleguide_builder.menu_links.yml';
          _styleguide_builder_import_init($path);

        }
