<?php

/**
 * @file
 * Code for the styleguide_builder module.
 *
 * The id and class values in this template are required for configuration
 * options to function correctly.
 *
 * @see template_preprocess()
 * @see template_preprocess_styleguide_builder_page()
 *
 * @ingroup themeable
 */
?>
<div id="style-guide" class="clearfix">
  <div class="style-guide-nav">
    <?php print render($menu['utility']); ?>
    <?php print render($menu['style_guide']); ?>
  </div>
  <div class="style-guide-elements">
    <?php print render($elements); ?>
  </div>
</div>
